@extends('adminlte::page')

@section('title', 'Direktori')

@section('content_header')
    <h1 class="m-0 text-dark">Direktori</h1>
@stop

@section('content')

    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @can('mahasiswa')
                        @livewire('direktori-mahasiswa')
                    @elsecan('organisasi')
                        @livewire('direktori-organisasi')
                    @elsecan('dosen')
                        @livewire('direktori-dosen')
                    @endcan
                </div>
            </div>
        </div>
    </div>
@stop
