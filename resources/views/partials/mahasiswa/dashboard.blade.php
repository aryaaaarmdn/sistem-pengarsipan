@extends('adminlte::page')

@section('title', 'Sistem Pengarsipan - Dashboard')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="card-deck">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Berkas</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_berkas }} Berkas</small>
                </p>
            </div>
        </div>
    </div>
@stop
