@extends('master.upload-berkas')

@section('title', 'Form Upload Berkas')

@section('judul', 'Upload Berkas')

@section('form')
    <form method="post" action="{{ route('upload-berkas.store') }}" enctype="multipart/form-data">
        @csrf

        {{-- Pesan error ketika terjadi kesalahan saat proses simpan data ke db --}}
        @if (session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @endif

        {{-- Pesan error ketika terjadi kesalahan saat upload / hapus file berkas --}}
        @if (isset($fail))
            <div class="alert alert-danger">
                {{ $fail }}
            </div>
        @endif

        <div class="form-group">
            <label for="nama_berkas">Nama Berkas</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" 
                id="nama_berkas" name="nama" value="{{ old('nama') }}">
            @error('nama')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="file">File Berkas</label>
            <input type="file" class="form-control-file @error('file') is-invalid @enderror" 
                id="file" name="file">
            @error('file')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea id="keterangan" class="form-control @error('keterangan') is-invalid @enderror" 
                name="keterangan" rows="4" cols="50">
                {{ old('keterangan') }}
            </textarea>
            @error('keterangan')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <button class="btn btn-primary">Simpan</button>
    </form>
@endsection
