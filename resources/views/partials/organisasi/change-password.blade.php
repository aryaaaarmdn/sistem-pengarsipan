@extends('master.upload-berkas')

@section('title', 'Ubah Password')

@section('judul', 'Ubah Password')

@section('form')
    <form method="post" action="{{ route('ubah-password.store') }}">
        @csrf

        @if(session('msg'))
            <div class="alert alert-success">
                {{ session('msg') }}        
            </div>
        @endif
        
        <div class="form-group">
            <label for="password_lama">Password Lama</label>
            <input type="password" 
                class="form-control @error('password_lama') is-invalid @enderror" 
                id="password_lama" name="password_lama">
            @error('password_lama')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="password_baru">Password Baru</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" 
                id="password_baru" name="password">
            @error('password')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="password_baru_confirm">Konfirmasi Password Baru</label>
            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" 
                id="password_baru_confirm" name="password_confirmation">
            @error('password_confirmation')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <button class="btn btn-primary">Simpan</button>
    </form>
@endsection