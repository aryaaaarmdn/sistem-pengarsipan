@extends('adminlte::page')

@section('title', 'Sistem Pengarsipan - Dashboard')

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')
    <div class="card-deck">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Surat</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_surat }} Surat</small>
                </p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Surat Masuk Internal</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_surat_masuk_internal }} Surat</small>
                </p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Surat Masuk Eksternal</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_surat_masuk_eksternal }} Surat</small>
                </p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Surat Keluar Internal</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_surat_keluar_internal }} Surat</small>
                </p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Total Surat Keluar Eksternal</h5>
                <p class="card-text">
                    <small class="text-muted">{{ $total_surat_keluar_eksternal }} Surat</small>
                </p>
            </div>
        </div>
    </div>
@stop
