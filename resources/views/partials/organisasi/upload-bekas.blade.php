@extends('master.upload-berkas')

@section('title', 'Form Upload Berkas')

@section('judul', 'Upload Berkas')

@section('form')
    <form method="post" action="{{ route('upload-berkas.store') }}" enctype="multipart/form-data">
        @csrf

        {{-- Pesan error ketika terjadi kesalahan saat proses simpan data ke db --}}
        @if (session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @endif

        {{-- Pesan error ketika terjadi kesalahan saat upload / hapus file berkas --}}
        @if (isset($fail))
            <div class="alert alert-danger">
                {{ $fail }}
            </div>
        @endif
        
        <div class="form-group">
            <label for="surat_asal">Asal Surat</label>
            <input type="text" 
                class="form-control @error('asal_surat') is-invalid @enderror" 
                id="surat_asal" name="asal_surat" value="{{ old('asal_surat') }}">
            @error('asal_surat')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="surat_tujuan">Tujuan Surat</label>
            <input type="text" class="form-control @error('tujuan_surat') is-invalid @enderror" 
                id="surat_tujuan" name="tujuan_surat" value="{{ old('tujuan_surat') }}">
            @error('tujuan_surat')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="jenis_surat">Jenis Surat</label>
            <select class="form-control @error('jenis_surat') is-invalid @enderror" 
                id="jenis_surat" name="jenis_surat">
                <option value="">--Pilih Jenis Surat--</option>
                @foreach ($jenis_surat as $js)
                    <option value="{{ $js->id }}" {{ old('jenis_surat') == $js->id ? 'selected' : '' }}>
                        {{ $js->nama }}
                    </option>
                @endforeach
            </select>
            @error('jenis_surat')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="keterangan_surat">Keterangan Surat</label>
            <select class="form-control @error('keterangan_surat') is-invalid @enderror" 
                id="keterangan_surat" name="keterangan_surat">
                <option value="">--Pilih Keterangan Surat--</option>
                @foreach ($keterangan_surat as $ks)
                    <option value="{{ $ks->id }}" {{ old('keterangan_surat') == $ks->id ? 'selected' : '' }}>
                        {{ $ks->nama }}
                    </option>
                @endforeach
            </select>
            @error('keterangan_surat')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="nama">Nama Berkas</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" 
                id="nama_berkas" name="nama" value="{{ old('nama') }}">
            @error('nama')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="file">File Berkas</label>
            <input type="file" class="form-control-file @error('file') is-invalid @enderror" 
                id="file" name="file">
            @error('file')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <div class="form-group">
            <label for="tgl_surat">Tanggal Surat</label>
            <input type="date" 
                class="form-control-file @error('tanggal_surat') is-invalid @enderror" 
                id="tgl_surat" name="tanggal_surat" value="{{ old('tanggal_surat') }}">
            @error('tanggal_surat')
                <small class="text-danger">
                    {{ $message }}
                </small>
            @enderror
        </div>
        <button class="btn btn-primary">Simpan</button>
    </form>
@endsection
