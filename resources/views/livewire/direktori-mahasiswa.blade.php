<div class="table-responsive">
    <div class="form-row mb-2">
        <div class="col-6">
            <input type="text" placeholder="Cari Berkas.." class="form-control" 
                wire:model.debounce.500ms="search"
            >
        </div>
    </div>
    <table class="table table-bordered table-hover">
        <thead>
            <tr class="text-center">
                <th scope="col">Nama Berkas</th>
                <th scope="col">Berkas</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Terakhir Diperbarui</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $_data)
                <tr class="text-center">
                    <td>{{ $_data->nama_berkas }}</td>
                    <td>
                        <div class="d-flex justify-content-center align-items-center">
                            <a href="{{ asset('storage/' . $_data->berkas) }}" 
                                target="_blank">
                                <i class="far fa-fw fa-file"></i>
                            </a>
                        </div>
                    </td>
                    <td>{{ $_data->keterangan }}</td>
                    <td>{{ $_data->updated_at }}</td>
                    <td>
                        <a href="{{ route('berkas.edit', [
                            'id' => $_data->id
                        ]) }}" class="btn btn-sm btn-primary mb-1">
                            Edit
                        </a>
                        <form method="POST" action="{{ route('berkas.delete', [
                            'id' => $_data->id
                        ]) }}">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-sm btn-danger mb-1" 
                                type="submit"
                                onclick="return confirm('Yakin ingin menghapus data ini?')"
                            >
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9" class="text-center">
                        <i><b>Tidak ada data</b></i>
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $data->links() }}
</div>