<?php

namespace App\Providers;

use App\Contracts\Loginable;
use App\Services\DosenAuthenticate;
use App\Services\MahasiswaAuthenticate;
use App\Services\OrganisasiAuthenticate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('organisasi', function ($user) {
            return auth()->guard('organisasi')->check();
        });

        Gate::define('mahasiswa', function ($user) {
            return auth()->guard('mahasiswa')->check();
        });

        Gate::define('dosen', function ($user) {
            return auth()->guard('dosen')->check();
        });
    }

    public function register(): void
    {
        $tipe = request('tipe');

        if ($tipe == 'organisasi') {
            $this->app->bind(Loginable::class, OrganisasiAuthenticate::class);
        } elseif ($tipe == 'dosen') {
            $this->app->bind(Loginable::class, DosenAuthenticate::class);
        } elseif ($tipe == 'mahasiswa') {
            $this->app->bind(Loginable::class, MahasiswaAuthenticate::class);
        }
    }
}
