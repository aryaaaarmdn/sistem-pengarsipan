<?php

namespace App\Services;

use App\Contracts\Loginable;
use App\Contracts\Registerable;
use App\Models\Dosen;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class DosenAuthenticate implements Loginable, Registerable
{
    public function login($username, $password)
    {
        $response = $this->hitApi($username, $password); // Hit api login

        if ($response->successful()) { // Jika response sukses

            // Ambil data response dan masukkan kedalam variable
            $nama = $response->json()['nama lengkap'];
            $kodedosen = $response->json()['KODEDOSEN'];
            $prodi = $response->json()['prodi']['nama_program_studi'];
            $kode_prodi = $response->json()['KODEPROGDI'];
            $fakultas = $response->json()['prodi']['jurusan']['fakultas']['nama_fakultas'];
            $kode_fakultas = $response->json()['prodi']['jurusan']['fakultas']['KODEFAK'];
            $singkatan_fakultas = $response->json()['prodi']['jurusan']['fakultas']['singkatan_fakultas'];

            return $this->checkDB(
                $nama,
                $kodedosen,
                $kode_prodi,
                $kode_fakultas,
                $prodi,
                $fakultas,
                $singkatan_fakultas
            );
        } elseif ($response->failed()) { // Jika response gagal, redirect beserta pesan error
            return false;
        }
    }

    public function registerAccount(
        $nama,
        $kodedosen,
        $prodi,
        $fakultas,
        $kodeProdi,
        $kodeFak,
        $fakultasSingkatan
    ) {
        return Dosen::updateOrCreate(
            [
                'nama' => $nama,
                'kodedosen' => $kodedosen,
            ],
            [
                'program_studi' => $prodi,
                'fakultas' => $fakultas,
                'kode_prodi' => $kodeProdi,
                'kode_fakultas' => $kodeFak,
                'fakultas_singkatan' => $fakultasSingkatan,
            ]
        );
    }

    public function hitApi($username, $password)
    {
        $response = Http::withHeaders([
            'Token' => '$2y$10$2vU.b4RlbrveaYdE0.gVX.0ALHehkq3.bi4KRzPJquWJByF5LjEzO',
        ])->post('https://main-api.uhamka.ac.id/api/get/login-dosen', [
            'kodedosen' => $username,
            'pwd' => $password
        ]);

        return $response;
    }

    public function checkDB(
        $nama,
        $kodedosen,
        $kodeProdi,
        $kodeFak,
        $prodi,
        $fakultas,
        $fakultasSingkatan
    ) {
        // Cek data pada db
        $data = Dosen::where('nama', $nama)
            ->where('kodedosen', $kodedosen)
            ->where('kode_prodi', $kodeProdi)
            ->where('kode_fakultas', $kodeFak)
            ->firstOr(function () {
                return null;
            });

        if ($data) { // Jika data ada
            Auth::guard('dosen')->login($data); // Lakukan login sbg mhs
        } else { // Jika data tidak ada
            // insert data mahasiswa dari response api sebelumnya pada database
            $data = $this->registerAccount(
                $nama,
                $kodedosen,
                $prodi,
                $fakultas,
                $kodeProdi,
                $kodeFak,
                $fakultasSingkatan
            );

            // Login sbg mhs
            Auth::guard('dosen')->login($data);
        }

        return true;
    }
}
