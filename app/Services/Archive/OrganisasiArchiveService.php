<?php

namespace App\Services\Archive;

use App\Contracts\ArchivableRequest;
use App\Models\ArsipSuratOrganisasi;
use App\Models\Organisasi\JenisSurat;
use App\Models\Organisasi\KeteranganSurat;
use App\Traits\FileAction;
use Illuminate\Validation\Rule;

class OrganisasiArchiveService implements ArchivableRequest
{
    use FileAction {
        delete as deleteFile;
    }

    private $path, $jenis_surat, $keterangan_surat;

    public function __construct()
    {
        $this->jenis_surat = JenisSurat::all();
        $this->keterangan_surat = KeteranganSurat::all();
        $this->path =
            'organisasi/' . auth()->user()->username . '/';
    }

    public function create()
    {
        return view('partials.organisasi.upload-bekas', [
            'jenis_surat' => $this->jenis_surat,
            'keterangan_surat' => $this->keterangan_surat,
        ]);
    }

    public function validation($request)
    {
        $request->validate([
            'asal_surat' => 'required',
            'tujuan_surat' => 'required',
            'jenis_surat' => [
                'required',
                Rule::in(\App\Models\Organisasi\JenisSurat::all()->pluck('id'))
            ],
            'keterangan_surat' => [
                'required',
                Rule::in(\App\Models\Organisasi\KeteranganSurat::all()->pluck('id'))
            ],
            'nama' => 'required',
            'file' => [
                Rule::requiredIf($request->method() == 'POST'),
                'file',
                'mimes:pdf,doc,docx',
                'max:5000'
            ],
            'tanggal_surat' => 'required|date',
        ], [
            'asal_surat.required' => 'Asal Surat Tidak Boleh Kosong',
            'tujuan_surat.required' => 'Tujuan Surat Tidak Boleh Kosong',
            'jenis_surat.required' => 'Jenis Surat Tidak Boleh Kosong',
            'jenis_surat.in' => 'Jenis Surat Tidak Valid',
            'keterangan_surat.required' => 'Keterangan Surat Tidak Boleh Kosong',
            'keterangan_surat.in' => 'Keterangan Surat Tidak Valid',
            'nama.required' => 'Nama Berkas Tidak Boleh Kosong',
            'file.required' => 'File Tidak Boleh Kosong',
            'file.file' => 'File harus berbentuk dokumen',
            'file.mimes' => 'Tipe file harus salah satu dari :values',
            'file.max' => 'Ukuran File Tidak Boleh Lebih dari :max kilobytes',
            'tanggal_surat.required' => 'Tanggal File Tidak Boleh Kosong',
            'tanggal_surat.date' => 'Tanggal Surat Tidak Valid'
        ]);
    }

    public function store($request)
    {
        // Validasi Request
        $this->validation($request);

        // Upload File
        $this->upload($request->file);

        // Simpan data request ke DB
        try {
            ArsipSuratOrganisasi::create([
                'user_organisasi_id' => auth()->user()->id,
                'asal_surat' => $request->asal_surat,
                'tujuan_surat' => $request->tujuan_surat,
                'nama_berkas_upload' => $this->path . $request->file('file')->hashName(),
                'nama_berkas' => $request->nama,
                'tanggal_surat' => $request->tanggal_surat,
                'keterangan_surat_id' => $request->keterangan_surat,
                'jenis_surat_id' => $request->jenis_surat,
            ]);
            return redirect()
                ->route('direktori')
                ->with('success', 'Berhasil Upload Berkas');
        } catch (\Illuminate\Database\QueryException $exception) {
            $this->delete($request->file('file')->hashName());
            return redirect()
                ->route('upload-berkas')
                ->with('fail', 'Terjadi Kesalahan (' . $exception->errorInfo[2] . ')');
        }
    }

    public function edit($id)
    {
        $data = ArsipSuratOrganisasi::where('id', $id)->firstOrFail();

        return view('partials.organisasi.edit-berkas', [
            'data' => $data,
            'jenis_surat' => $this->jenis_surat,
            'keterangan_surat' => $this->keterangan_surat,
        ]);
    }

    public function update($id, $request)
    {
        // Dapatkan data model yang ingin diupdate
        $modelWantUpdate = ArsipSuratOrganisasi::where('id', $id)->firstOrFail();

        // Validasi
        $this->validation($request);

        // Jika user mengupload berkas baru
        if ($request->has('file')) {
            // Hapus Berkas Lama
            $this->deleteFile($modelWantUpdate->nama_berkas_upload);

            // Upload Berkas Baru
            $this->upload($request->file);
        }

        // Update Data
        try {
            $modelWantUpdate->update([
                'user_organisasi_id' => auth()->user()->id,
                'asal_surat' => $request->asal_surat,
                'tujuan_surat' => $request->tujuan_surat,
                'nama_berkas_upload' =>
                $request->has('file') ?
                    $this->path . $request->file('file')->hashName() :
                    $modelWantUpdate->nama_berkas_upload,
                'nama_berkas' => $request->nama,
                'tanggal_surat' => $request->tanggal_surat,
                'keterangan_surat_id' => $request->keterangan_surat,
                'jenis_surat_id' => $request->jenis_surat,
            ]);

            return redirect()
                ->route('direktori')
                ->with('success', 'Berhasil Perbarui Berkas');
        } catch (\Illuminate\Database\QueryException $exception) {
            if ($request->has('file')) {
                $this->delete($request->file('file')->hashName());
            }

            return redirect()
                ->back()
                ->with('fail', 'Terjadi Kesalahan (' . $exception->errorInfo[2] . ')');
        }
    }

    public function delete($id)
    {
        $model = ArsipSuratOrganisasi::findOrFail($id);

        $this->deleteFile($model->nama_berkas_upload);

        $model->delete();
    }
}
