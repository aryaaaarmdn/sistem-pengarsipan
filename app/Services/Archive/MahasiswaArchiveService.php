<?php

namespace App\Services\Archive;

use App\Contracts\ArchivableRequest;
use App\Models\ArsipBerkasMahasiswa;
use App\Traits\FileAction;

use Illuminate\Validation\Rule;

class MahasiswaArchiveService implements ArchivableRequest
{
    use FileAction {
        delete as deleteFile;
    }

    private $path;

    public function __construct()
    {
        $this->path =
            'mahasiswa/' . auth()->user()->nim . '/';
    }

    public function create()
    {
        return view('partials.mahasiswa.upload-berkas');
    }

    public function validation($request)
    {
        $request->validate([
            'nama' => 'required',
            'keterangan' => 'nullable',
            'file' => [
                Rule::requiredIf($request->method() == 'POST'),
                'file',
                'mimes:pdf,doc,docx',
                'max:5000'
            ],
        ]);
    }

    public function store($request)
    {
        // Validasi Request
        $this->validation($request);

        // Upload File
        $this->upload($request->file);

        // Simpan data request ke DB
        try {
            ArsipBerkasMahasiswa::create([
                'nim' => auth()->user()->nim,
                'nama_berkas' => $request->nama,
                'berkas' => $this->path . $request->file('file')->hashName(),
                'keterangan' => $request->has('keterangan') ? $request->keterangan : null,
            ]);
            return redirect()
                ->route('direktori')
                ->with('success', 'Berhasil Upload Berkas');
        } catch (\Illuminate\Database\QueryException $exception) {
            $this->delete($request->file('file')->hashName());
            return redirect()
                ->route('upload-berkas')
                ->with('fail', 'Terjadi Kesalahan (' . $exception->errorInfo[2] . ')');
        }
    }

    public function edit($id)
    {
        $data = ArsipBerkasMahasiswa::where('id', $id)->firstOrFail();

        return view('partials.mahasiswa.edit-berkas', [
            'data' => $data,
        ]);
    }

    public function update($id, $request)
    {
        // dd($request);
        // Dapatkan data model yang ingin diupdate
        $modelWantUpdate = ArsipBerkasMahasiswa::where('id', $id)->firstOrFail();

        // Validasi
        $this->validation($request);

        // Jika user mengupload berkas baru
        if ($request->has('file')) {
            // Hapus Berkas Lama
            $this->deleteFile($modelWantUpdate->berkas);

            // Upload Berkas Baru
            $this->upload($request->file);
        }

        // Update Data
        try {
            $modelWantUpdate->update([
                'berkas' => $request->has('file') ?
                    $this->path . $request->file('file')->hashName() :
                    $modelWantUpdate->berkas,
                'nama_berkas' => $request->nama,
                'keterangan' => $request->has('keterangan') ? $request->keterangan : null,
            ]);

            return redirect()
                ->route('direktori')
                ->with('success', 'Berhasil Perbarui Berkas');
        } catch (\Illuminate\Database\QueryException $exception) {
            if ($request->has('file')) {
                $this->delete($request->file('file')->hashName());
            }

            return redirect()
                ->back()
                ->with('fail', 'Terjadi Kesalahan (' . $exception->errorInfo[2] . ')');
        }
    }

    public function delete($id)
    {
        $model = ArsipBerkasMahasiswa::findOrFail($id);

        $this->deleteFile($model->berkas);

        $model->delete();
    }
}
