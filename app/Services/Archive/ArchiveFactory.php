<?php

namespace App\Services\Archive;

use App\Services\Archive\MahasiswaArchiveService;
use App\Services\Archive\OrganisasiArchiveService;

use InvalidArgumentException;

class ArchiveFactory
{
    private static $mapping = [
        'mahasiswa' => MahasiswaArchiveService::class,
        'organisasi' => OrganisasiArchiveService::class,
    ];

    public static function getInstance($guard)
    {
        if (!array_key_exists($guard, self::$mapping)) {
            throw new InvalidArgumentException(
                'Argumen tidak sesuai'
            );
        }

        return new self::$mapping[$guard]();
    }
}
