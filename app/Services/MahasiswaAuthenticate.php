<?php

namespace App\Services;

use App\Contracts\Loginable;
use App\Contracts\Registerable;
use App\Models\Mahasiswa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class MahasiswaAuthenticate implements Loginable, Registerable
{
    public function login($username, $password)
    {
        $response = $this->hitApi($username, $password); // Hit api login

        if ($response->successful()) { // Jika response sukses

            // Ambil data response dan masukkan kedalam variable
            $nama = $response->json()['nama lengkap'];
            $nim = $response->json()['nim'];
            $prodi = $response->json()['prodi']['nama_program_studi'];
            $kode_prodi = $response->json()['KODEPROGDI'];
            $fakultas = $response->json()['prodi']['jurusan']['fakultas']['nama_fakultas'];
            $kode_fakultas = $response->json()['prodi']['jurusan']['fakultas']['KODEFAK'];
            $singkatan_fakultas = $response->json()['prodi']['jurusan']['fakultas']['singkatan_fakultas'];

            return $this->checkDB(
                $nama,
                $nim,
                $kode_prodi,
                $kode_fakultas,
                $prodi,
                $fakultas,
                $singkatan_fakultas
            );
        } elseif ($response->failed()) { // Jika response gagal, redirect beserta pesan error
            return false;
        }
    }

    public function checkDB(
        $nama,
        $nim,
        $kode_prodi,
        $kode_fakultas,
        $prodi,
        $fakultas,
        $fakultasSingkatan
    ) {
        // Cek data pada db
        $data = Mahasiswa::where('nama', $nama)
            ->where('nim', $nim)
            ->where('kode_prodi', $kode_prodi)
            ->where('kode_fakultas', $kode_fakultas)
            ->firstOr(function () {
                return null;
            });

        if ($data) { // Jika data ada
            Auth::guard('mahasiswa')->login($data); // Lakukan login sbg mhs
        } else { // Jika data tidak ada
            // insert data mahasiswa dari response api sebelumnya pada database
            $data = $this->registerAccount(
                $nama,
                $nim,
                $prodi,
                $fakultas,
                $kode_prodi,
                $kode_fakultas,
                $fakultasSingkatan
            );

            // Login sbg mhs
            Auth::guard('mahasiswa')->login($data);
        }

        return true;
    }

    public function registerAccount(
        $nama,
        $nim,
        $prodi,
        $fakultas,
        $kodeProdi,
        $kodeFak,
        $fakultasSingkatan
    ) {
        return Mahasiswa::updateOrCreate(
            [
                'nama' => $nama,
                'nim' => $nim,
            ],
            [
                'program_studi' => $prodi,
                'fakultas' => $fakultas,
                'kode_prodi' => $kodeProdi,
                'kode_fakultas' => $kodeFak,
                'fakultas_singkatan' => $fakultasSingkatan,
            ]
        );
    }

    public function hitApi($username, $password)
    {
        $response = Http::withHeaders([
            'Token' => '$2y$10$2vU.b4RlbrveaYdE0.gVX.0ALHehkq3.bi4KRzPJquWJByF5LjEzO',
        ])->post('https://main-api.uhamka.ac.id/api/get/login-mhs', [
            'nim' => $username,
            'pwd' => $password
        ]);

        return $response;
    }
}
