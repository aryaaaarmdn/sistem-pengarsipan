<?php

namespace App\Services\Dashboard;

use App\Models\ArsipBerkasMahasiswa;

class MahasiswaDashboard extends DashboardService
{
    public function view()
    {
        $total_berkas = ArsipBerkasMahasiswa::user()->count();
        return view('partials.mahasiswa.dashboard', [
            'total_berkas' => $total_berkas,
        ]);
    }
}
