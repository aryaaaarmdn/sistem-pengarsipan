<?php

namespace App\Services\Dashboard;

use App\Models\ArsipSuratOrganisasi;

class OrganisasiDashboard extends DashboardService
{
    public function view()
    {
        $total_surat = ArsipSuratOrganisasi::user()->count();
        $total_surat_masuk_internal = ArsipSuratOrganisasi::user()->where('jenis_surat_id', 1)->count();
        $total_surat_masuk_eksternal = ArsipSuratOrganisasi::user()->where('jenis_surat_id', 2)->count();
        $total_surat_keluar_internal = ArsipSuratOrganisasi::user()->where('jenis_surat_id', 3)->count();
        $total_surat_keluar_eksternal = ArsipSuratOrganisasi::user()->where('jenis_surat_id', 4)->count();

        return view('partials.organisasi.dashboard', [
            'total_surat' => $total_surat,
            'total_surat_masuk_internal' => $total_surat_masuk_internal,
            'total_surat_masuk_eksternal' => $total_surat_masuk_eksternal,
            'total_surat_keluar_internal' => $total_surat_keluar_internal,
            'total_surat_keluar_eksternal' => $total_surat_keluar_eksternal,
        ]);
    }
}
