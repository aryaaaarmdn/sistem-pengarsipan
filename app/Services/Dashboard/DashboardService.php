<?php

namespace App\Services\Dashboard;

abstract class DashboardService
{
    abstract public function view();
}
