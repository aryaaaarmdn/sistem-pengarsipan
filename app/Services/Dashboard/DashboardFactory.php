<?php

namespace App\Services\Dashboard;

use InvalidArgumentException;

class DashboardFactory
{
    private static $mapping = [
        'mahasiswa' => MahasiswaDashboard::class,
        'organisasi' => OrganisasiDashboard::class,
        'dosen' => DosenDashboard::class,
    ];

    public static function getInstance($guard)
    {
        if (!array_key_exists($guard, self::$mapping)) {
            throw new InvalidArgumentException(
                'Argumen tidak sesuai'
            );
        }

        return new self::$mapping[$guard]();
    }
}
