<?php

namespace App\Services\Dashboard;

use App\Models\ArsipBerkasMahasiswa;
use App\Models\Mahasiswa;

class DosenDashboard extends DashboardService
{
    public function view()
    {
        $total_mahasiswa = Mahasiswa::whereHas('berkas')->count();

        $total_berkas = ArsipBerkasMahasiswa::count();

        return view('partials.dosen.dashboard', [
            'total_mahasiswa' => $total_mahasiswa,
            'total_berkas' => $total_berkas,
        ]);
    }
}
