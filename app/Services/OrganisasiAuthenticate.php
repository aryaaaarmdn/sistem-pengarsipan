<?php

namespace App\Services;

use App\Contracts\Loginable;

class OrganisasiAuthenticate implements Loginable
{
    public function login($username, $password)
    {
        if (auth()->guard('organisasi')->attempt([
            'username' => $username,
            'password' => $password
        ])) {
            return true;
        }

        return false;
    }
}
