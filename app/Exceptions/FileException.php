<?php

namespace App\Exceptions;

use Exception;

class FileException extends Exception
{
    public function render()
    {
        return redirect()->route('direktori', [
            'fail' => $this->getMessage()
        ], 500);
    }
}
