<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mahasiswa extends Authenticatable
{
    use HasFactory;

    protected $table = 'mahasiswa';
    public $timestamps = false;
    protected $guarded = [];

    public function berkas()
    {
        return $this->hasMany(ArsipBerkasMahasiswa::class, 'nim', 'nim');
    }
}
