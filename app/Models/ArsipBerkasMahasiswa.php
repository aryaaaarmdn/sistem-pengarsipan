<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Builder;

class ArsipBerkasMahasiswa extends Model
{
    use HasFactory;

    protected $table = 'arsip_berkas_mahasiswa';
    protected $guarded = [];

    public function scopeUser(Builder $query)
    {
        $query->where('nim', auth()->user()->nim);
    }

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'nim', 'nim')->withDefault();
    }
}
