<?php

namespace App\Models\Organisasi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeteranganSurat extends Model
{
    use HasFactory;

    protected $table = 'keterangan_surat';
    protected $guarded = [];
    public $timestamps = false;
}
