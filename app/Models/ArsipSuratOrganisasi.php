<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ArsipSuratOrganisasi extends Model
{
    use HasFactory;

    protected $table = 'arsip_surat_organisasi';
    protected $guarded = [];

    public function jenis_surat()
    {
        return $this->belongsTo(Organisasi\JenisSurat::class);
    }

    public function keterangan_surat()
    {
        return $this->belongsTo(Organisasi\KeteranganSurat::class);
    }

    public function scopeUser(Builder $query)
    {
        $query->where('user_organisasi_id', auth()->user()->id);
    }
}
