<?php

namespace App\Traits;

use App\Exceptions\FileException;

trait FileAction
{
    public function upload($file)
    {
        if ($file->getError() != 0) {
            throw new FileException('Terjadi Kesalahan Saat Upload Berkas');
        }
        $file->store($this->path);
    }

    public function delete($file)
    {
        if (!file_exists(public_path('storage/' . $file))) {
            throw new FileException('Terjadi Kesalahan Saat Menghapus Berkas');
        }

        unlink(public_path('storage/' . $file));
    }
}
