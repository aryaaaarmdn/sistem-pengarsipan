<?php

if (!function_exists('getGuard')) {
    function getGuard()
    {
        $guards = array_keys(config('auth.guards'));

        return collect($guards)->first(function ($guard) {
            return auth()->guard($guard)->check();
        });
    }
}
