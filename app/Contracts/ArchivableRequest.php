<?php

namespace App\Contracts;

interface ArchivableRequest
{
    public function create();
    public function validation($request);
    public function store($request);
    public function edit($id);
    public function update($id, $request);
    public function delete($id);
}
