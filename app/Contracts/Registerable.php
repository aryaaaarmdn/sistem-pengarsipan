<?php

namespace App\Contracts;

interface Registerable
{
    public function checkDB($nama, $identifier, $kodeProdi, $kodeFak, $prodi, $fakultas, $fakultasSingkatan);
    public function registerAccount($nama, $identifier, $prodi, $fakultas, $kodeProdi, $kodeFak, $fakultasSingkatan);
    public function hitApi($username, $password);
}
