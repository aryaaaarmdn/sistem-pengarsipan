<?php

namespace App\Contracts;

interface Loginable
{
    const errMsg = 'Data kredensial tidak cocok dengan database';

    public function login($username, $password);
}
