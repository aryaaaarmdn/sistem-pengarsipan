<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;

use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:organisasi');
    }

    public function index()
    {
        return view('partials.organisasi.change-password');
    }

    public function store(ChangePasswordRequest $request)
    {
        // Ambil data user yang sedang login
        $user = auth()->user();

        // cek jika password saat ini (di database) cocok dengan inputan user password lama
        if (Hash::check($request->password_lama, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->back()->with('msg', 'Berhasil Ganti Password');
        }
    }
}
