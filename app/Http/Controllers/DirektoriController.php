<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DirektoriController extends Controller
{
    public function index()
    {
        return view('master.direktori');
    }
}
