<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class CobaController extends Controller
{
    public function index()
    {
        dd([
            'guard' => auth()->guard(),
            'auth' => auth()->check(),
            'default' => auth()->user(),
            'auth_user' => auth()->user(),
            'organisasi_check' => auth()->guard('organisasi')->check(),
            'organisasi_user' => auth()->guard('organisasi')->user(),
            'dosen_check' => auth()->guard('dosen')->check(),
            'dosen_user' => auth()->guard('dosen')->user(),
            'mahasiswa_check' => auth()->guard('mahasiswa')->check(),
            'mahasiswa_user' => auth()->guard('mahasiswa')->user(),
        ]);
    }

    public function coba2()
    {
        dd(Artisan::call('migrate:status'));
    }
}
