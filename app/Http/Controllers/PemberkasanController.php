<?php

namespace App\Http\Controllers;

use App\Services\Archive\ArchiveFactory;
use Illuminate\Http\Request;

class PemberkasanController extends Controller
{
    private $archive_service;

    public function __construct()
    {
        // Untuk mengecualikan/tidak memperbolehkan dosen untuk mengakses controller ini
        $this->middleware('auth:organisasi,mahasiswa');

        // Untuk menentukan class service mahasiswa atau organisasi dalam menghandle pemberkasan
        $this->middleware(function ($request, $next) {
            $guard = getGuard();
            $this->archive_service = ArchiveFactory::getInstance($guard);

            return $next($request);
        });
    }

    public function create()
    {
        return $this->archive_service->create();
    }

    public function store(Request $request)
    {
        return $this->archive_service->store($request);
    }

    public function delete($id)
    {
        $this->archive_service->delete($id);

        return redirect()
            ->route('direktori')
            ->with('success', 'Berhasil Hapus Data Berkas');
    }

    public function edit($id)
    {
        return $this->archive_service->edit($id);
    }

    public function update($id, Request $request)
    {
        return $this->archive_service->update($id, $request);
    }
}
