<?php

namespace App\Http\Controllers;

use App\Services\Dashboard\DashboardFactory;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $guard = getGuard();
            $this->service = DashboardFactory::getInstance($guard);

            return $next($request);
        });
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return $this->service->view();
    }
}
