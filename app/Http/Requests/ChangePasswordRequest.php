<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->guard('organisasi')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        // Ambil data password pada database
        $password = auth()->user()->password;

        return [
            'password_lama' => [
                'required',
                // Cek jika inputan password lama tidak sesuai dengan database
                function ($attribute, $value, $fail) use ($password) {
                    if (!Hash::check($value, $password)) {
                        $fail('Password lama tidak sesuai pada database');
                    }
                },
            ],
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'password_lama.required' => 'Password lama tidak boleh kosong',
            'password.required' => 'Password baru tidak boleh kosong',
            'password.confirmed' => 'Password baru tidak cocok dengan konfirmasi password baru'
        ];
    }
}
