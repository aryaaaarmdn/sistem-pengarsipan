<?php

namespace App\Http\Requests\Auth;

use App\Contracts\Loginable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class LoginRequest extends FormRequest
{
    private $auth;

    public function __construct(Loginable $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => ['required', 'string'],
            'tipe' => [
                Rule::in(['mahasiswa', 'dosen', 'organisasi']),
            ],
            'password' => ['required', 'string'],
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     */
    public function authenticate()
    {
        if (!$this->auth->login($this->username, $this->password)) {
            throw ValidationException::withMessages([
                'kredensial' => $this->auth::errMsg,
            ]);
        }
    }
}
