<?php

namespace App\Http\Livewire;

use App\Models\ArsipSuratOrganisasi;
use App\Models\Organisasi\JenisSurat;
use App\Models\Organisasi\KeteranganSurat;

use Livewire\Component;
use Livewire\WithPagination;

class DirektoriOrganisasi extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search, $jenis_surat, $keterangan_surat, $tahun_surat;
    public $startYear, $thisYear;

    public function updating()
    {
        $this->resetPage();
    }

    public function mount()
    {
        $this->startYear = 2000;
        $this->thisYear = date('Y');
    }

    public function render()
    {
        $data = $this->searching();
        $jenis_surat = JenisSurat::orderBy('kode')->get();
        $keterangan_surat = KeteranganSurat::orderBy('kode_surat')->get();

        return view('livewire.direktori-organisasi', [
            'data' => $data,
            'jenis__surat' => $jenis_surat,
            'keterangan__surat' => $keterangan_surat,
        ]);
    }

    private function searching()
    {
        $data = ArsipSuratOrganisasi::query();

        if (!empty($this->search)) {
            $data->where(function ($query) {
                $query->orWhere('asal_surat', 'LIKE', '%' . $this->search . '%')
                    ->orWhere('tujuan_surat', 'LIKE', '%' . $this->search . '%')
                    ->orWhere('nama_berkas', 'LIKE', '%' . $this->search . '%')
                    ->orWhere('tanggal_surat', 'LIKE', '%' . $this->search . '%')
                    ->orWhere('updated_at', 'LIKE', '%' . $this->search . '%');
            });
        }

        if (!empty($this->jenis_surat)) {
            $data->where('jenis_surat_id', $this->jenis_surat);
        }

        if (!empty($this->keterangan_surat)) {
            $data->where('keterangan_surat_id', $this->keterangan_surat);
        }

        if (!empty($this->tahun_surat)) {
            $data->whereYear('tanggal_surat', $this->tahun_surat);
        }

        return $data->user()->orderBy('updated_at', 'desc')->paginate(2);
    }
}
