<?php

namespace App\Http\Livewire;

use App\Models\ArsipBerkasMahasiswa;

use Livewire\Component;
use Livewire\WithPagination;

class DirektoriDosen extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;

    public function render()
    {
        $data = $this->getData();

        return view('livewire.direktori-dosen', [
            'data' => $data,
        ]);
    }

    public function updating()
    {
        $this->resetPage();
    }

    private function getData()
    {
        $data = ArsipBerkasMahasiswa::query();

        if (!empty($this->search)) {
            $data->whereHas('mahasiswa', function ($query) {
                $query->where('kode_fakultas', auth()->user()->kode_fakultas);
            })
                ->with([
                    'mahasiswa' => function ($query) {
                        $query->select(['id', 'nama', 'nim']);
                    }
                ])
                ->where(function ($query) {
                    $query->orWhere('nama_berkas', 'LIKE', '%' . $this->search . '%');
                    $query->orWhere('nim', 'LIKE', '%' . $this->search . '%');
                    $query->orWhere('keterangan', 'LIKE', '%' . $this->search . '%');
                    $query->orWhere(function ($query) {
                        $query->whereHas('mahasiswa', function ($query) {
                            $query->where(
                                'nama',
                                'LIKE',
                                '%' . $this->search . '%'
                            );
                        });
                    });
                });
        } elseif (empty($this->search)) {
            $data->whereHas('mahasiswa', function ($query) {
                $query->where('kode_fakultas', auth()->user()->kode_fakultas);
            })
                ->with([
                    'mahasiswa' => function ($query) {
                        $query->select(['id', 'nama', 'nim']);
                    }
                ]);
        }

        return $data->orderBy('updated_at', 'desc')->paginate(2);
    }
}
