<?php

namespace App\Http\Livewire;

use App\Models\ArsipBerkasMahasiswa;

use Livewire\Component;
use Livewire\WithPagination;

class DirektoriMahasiswa extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search;

    public function render()
    {
        $data = $this->getData();

        return view('livewire.direktori-mahasiswa', [
            'data' => $data,
        ]);
    }

    public function updating()
    {
        $this->resetPage();
    }

    private function getData()
    {
        $data = ArsipBerkasMahasiswa::query();

        if (!empty($this->search)) {
            $data->where(function ($query) {
                $query->orWhere('nama_berkas', 'LIKE', '%' . $this->search . '%')
                    ->orWhere('keterangan', 'LIKE', '%' . $this->search . '%');
            });
        }

        return $data->user()->orderBy('updated_at', 'desc')->paginate(2);
    }
}
