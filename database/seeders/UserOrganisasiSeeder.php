<?php

namespace Database\Seeders;

use App\Models\Organisasi;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserOrganisasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'id' => 1,
                'username' => 'himati',
                'password' => Hash::make('123'),
                'nama' => 'Himpunan Mahasiswa Teknik Informatika'
            ],
            [
                'id' => 2,
                'username' => 'himate',
                'password' => Hash::make('123'),
                'nama' => 'Himpunan Mahasiswa Teknik Elektro'
            ],
            [
                'id' => 3,
                'username' => 'himatm',
                'password' => Hash::make('123'),
                'nama' => 'Himpunan Mahasiswa Teknik Mesin'
            ],
            [
                'id' => 4,
                'username' => 'bemftii',
                'password' => Hash::make('123'),
                'nama' => 'Badan Eksekutif Fakultas Teknologi Industri & Informatika',
            ],
            [
                'id' => 5,
                'username' => 'dpmftii',
                'password' => Hash::make('123'),
                'nama' => 'Dewan Perwakilan Mahasiswa Fakultas Teknologi Industri & Informatika'
            ],
        ];

        foreach ($items as $item) {
            Organisasi::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
