<?php

namespace Database\Seeders;

use App\Models\Organisasi\KeteranganSurat;
use Illuminate\Database\Seeder;

class KeteranganSuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'id' => 1,
                'kode_surat' => 1,
                'nama' => 'Surat Ketetapan',
            ],
            [
                'id' => 2,
                'kode_surat' => 1,
                'nama' => 'Surat Keputusan',
            ],
            [
                'id' => 3,
                'kode_surat' => 2,
                'nama' => 'Surat Mandat',
            ],
            [
                'id' => 4,
                'kode_surat' => 2,
                'nama' => 'Surat Tugas',
            ],
            [
                'id' => 5,
                'kode_surat' => 2,
                'nama' => 'Surat Delegasi',
            ],
            [
                'id' => 6,
                'kode_surat' => 2,
                'nama' => 'Surat Instruksi',
            ],
            [
                'id' => 7,
                'kode_surat' => 3,
                'nama' => 'Surat Keterangan',
            ],
            [
                'id' => 8,
                'kode_surat' => 3,
                'nama' => 'Surat Sertifikat',
            ],
            [
                'id' => 9,
                'kode_surat' => 4,
                'nama' => 'Surat Undangan',
            ],
            [
                'id' => 10,
                'kode_surat' => 4,
                'nama' => 'Surat Pemberitahuan',
            ],
            [
                'id' => 11,
                'kode_surat' => 4,
                'nama' => 'Surat Edaran',
            ],
            [
                'id' => 12,
                'kode_surat' => 4,
                'nama' => 'Surat Himbauan',
            ],
            [
                'id' => 13,
                'kode_surat' => 5,
                'nama' => 'Surat Permohonan',
            ],
            [
                'id' => 14,
                'kode_surat' => 6,
                'nama' => 'Surat Memorandum',
            ],
            [
                'id' => 15,
                'kode_surat' => 6,
                'nama' => 'Surat Peringatan',
            ],
            [
                'id' => 16,
                'kode_surat' => 7,
                'nama' => 'Surat Pengantar',
            ],
            [
                'id' => 17,
                'kode_surat' => 8,
                'nama' => 'dll',
            ],
        ];

        foreach ($items as $item) {
            KeteranganSurat::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
