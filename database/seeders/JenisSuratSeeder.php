<?php

namespace Database\Seeders;

use App\Models\Organisasi\JenisSurat;
use Illuminate\Database\Seeder;

class JenisSuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'id' => 1,
                'kode' => 1,
                'nama' => 'Surat Masuk Internal'
            ],
            [
                'id' => 2,
                'kode' => 2,
                'nama' => 'Surat Masuk Eksternal'
            ],
            [
                'id' => 3,
                'kode' => 3,
                'nama' => 'Surat Keluar Internal'
            ],
            [
                'id' => 4,
                'kode' => 4,
                'nama' => 'Surat Keluar Eksternal'
            ]
        ];

        foreach ($items as $item) {
            JenisSurat::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
