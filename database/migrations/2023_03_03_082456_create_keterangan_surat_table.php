<?php

use Database\Seeders\KeteranganSuratSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('keterangan_surat', function (Blueprint $table) {
            $table->id();
            $table->string('kode_surat');
            $table->string('nama');
        });

        Artisan::call('db:seed', [
            '--class' => KeteranganSuratSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('keterangan_surat');
    }
};
