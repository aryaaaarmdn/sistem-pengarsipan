<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('arsip_surat_organisasi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_organisasi_id')->constrained('user_organisasi');
            $table->string('asal_surat');
            $table->string('tujuan_surat');
            $table->string('nama_berkas_upload');
            $table->string('nama_berkas');
            $table->date('tanggal_surat');
            $table->foreignId('keterangan_surat_id')->constrained('keterangan_surat');
            $table->foreignId('jenis_surat_id')->constrained('jenis_surat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('arsip_surat_organisasi');
    }
};
