<?php

use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DirektoriController;
use App\Http\Controllers\PemberkasanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:organisasi,dosen,mahasiswa'])->group(function () {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');

    Route::controller(PemberkasanController::class)->group(function () {
        Route::get('upload-berkas', 'create')->name('upload-berkas');
        Route::post('upload-berkas', 'store')->name('upload-berkas.store');
        Route::delete('berkas/delete/{id}', 'delete')->name('berkas.delete');
        Route::get('berkas/{id}/edit', 'edit')->name('berkas.edit');
        Route::put('berkas/{id}/update', 'update')->name('berkas.update');
    });

    Route::controller(DirektoriController::class)->group(function () {
        Route::get('direktori', 'index')->name('direktori');
    });

    Route::controller(ChangePasswordController::class)->group(function () {
        Route::get('ubah-password', 'index')->name('ubah-password.index');
        Route::post('ubah-password', 'store')->name('ubah-password.store');
    });
});

require __DIR__ . '/auth.php';
